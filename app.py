import logging

from fastapi import FastAPI
from fastapi_restful import Api
from sqlmodel import SQLModel

from config.db import engine
from resources.vehicle import GetVehicle

logging.basicConfig(
            filename='app.log', 
            filemode='w', 
            format='%(name)s - %(levelname)s - %(message)s'
            )

app = FastAPI()
api = Api(app)

SQLModel.metadata.create_all(engine)

api.add_resource(GetVehicle(), "/v1/vehicle/get/{vin_code:str}")
