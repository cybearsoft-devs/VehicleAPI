from typing import Optional
from sqlmodel import Field, SQLModel, Session, select

from config.db import engine


class VehicleModel(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    model_id: int
    manufacturer_id: int
    vin_code: str

    @classmethod
    def find_by_vin(cls, vin_code):
        with Session(engine) as session:
            statement = select(cls.id, cls.model_id, cls.manufacturer_id). \
                            where(VehicleModel.vin_code == vin_code)
            return session.exec(statement).first()
            
    def save_to_db(self):
        with Session(engine) as session:
            session.add(self)
            session.commit()

