from fastapi.testclient import TestClient
from app import app

client = TestClient(app)

def test_get_existing_vehicle():
    response = client.get("/v1/vehicle/get/asvfasbdfas", )
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "model_id": 1,
        "manufacturer_id": 1
        }

def test_get_unexisting_vehicle():
    response = client.get("/v1/vehicle/get/asvfasbdfas1")
    assert response.status_code == 404
    assert response.json() == {"detail": "Vehicle not found."}

def test_unknown_route():
    response = client.get("/v1/asvfasbdfas1")
    assert response.status_code == 404
    assert response.json() == {"detail": "Not Found"}