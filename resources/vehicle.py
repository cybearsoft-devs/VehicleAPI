import logging

from fastapi import HTTPException
from fastapi_restful import Resource

from models.vehicle import VehicleModel
from config.messages import VEHICLE_NOT_FOUND_ERROR, DB_EXTRACTION_ERROR


class GetVehicle(Resource):
    
    def get(self, vin_code: str):
        try:
            data = VehicleModel.find_by_vin(vin_code)
        except Exception as ex:
            logging.error(f"Could not get an response from db. Details: {ex}") 
            raise HTTPException(status_code=500, detail=DB_EXTRACTION_ERROR)

        if data: 
            return data
        else: 
            raise HTTPException(status_code=404, detail=VEHICLE_NOT_FOUND_ERROR)

        
            
